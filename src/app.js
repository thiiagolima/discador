const express = require('express');

// App
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// Load routes
const indexRoutes = require('./routes/index-routes');
app.use('/', indexRoutes);

const callRoutes = require('./routes/call-routes');
app.use('/call', callRoutes);
app.set('view engine', 'ejs');
app.use(express.static('assets'));
module.exports = app;