const express = require('express');
const router = express.Router();
const meetingController = require('../controllers/meeting-controller');

router.post('/valids', meetingController.validsRoutine);
router.get('/valids', meetingController.searchValids);
module.exports = router;