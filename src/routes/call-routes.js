const express = require('express');
const router = express.Router();
const callController = require('../controllers/call-controller');
const { Cluster } = require('puppeteer-cluster');
function generate(n) {
    var add = 1,
        max = 12 - add; // 12 is the min safe number Math.random() can generate without it starting to pad the end with zeros.   

    if (n > max) {
        return generate(max) + generate(n - max);
    }

    max = Math.pow(10, n + add);
    var min = max / 10; // Math.pow(10, n) basically
    var number = Math.floor(Math.random() * (max - min + 1)) + min;

    return ("" + number).substring(add);
}
(async () => {
    try {
        const cluster = await Cluster.launch({
            concurrency: Cluster.CONCURRENCY_BROWSER,
            maxConcurrency: 1,
            monitor: true,
            retryLimit: 3,
            timeout: 1000000,
            puppeteerOptions: {
                headless: false,
                args: [
                    "--no-sandbox",
                    "--disable-gpu",
                    "--disable-features=site-per-process",
                    //'--proxy-server=socks5://127.0.0.1:9150'
                ],
                //executablePath: puppeteerPath+'/.local-chromium/linux-818858/chrome-linux/chrome'
            },
        });
        await cluster.task(async ({ page, data }) => {
            const type = '1';
            const number = data.number;
            console.log(number)
            const s_number = number.split(':');
            const ddi = s_number[0];
            const call_number = s_number[1];
            tipo_empresa = 1;

            const NAME_INPUT = 'input[placeholder="Your full name"]';
            const NAME_INPUT2 = 'input[placeholder="Name"]';
            const EMAIL_INPUT = 'input[placeholder="Email address"]';
            const BUTTON_SELECTOR = '#guest_next-btn';
            const CALL_DIALOG = '#layoutdomid > div > div.style-dialog-body-2FRqj'
            const DISABLED_DIALOG = '#layoutdomid > div.style-modalbox-3IBZd > div > div'
            const BUTTON_ENTER_SELECTOR = '#interstitial_join_btn';
            const MUTE_SELECTOR = '#meetingSimpleContainer > div > div.style-control-bar-2vCte > div:nth-child(1) > div > button';
            const INFO_MEETING = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-loading-container-2yGDp.style-new-controlbar-2KcDQ > div.style-loading-text-box-3HRGa > div.style-room-content-box-36DZ8 > div.style-room-info-1QC-6'
            const BUTTON_AGREE = '#disclaimer_agree_btn'
            switch (type) {
                case '1':
                    AUDIO_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-audio-select-box-1LOYQ > button';
                    SKIP_SELECTOR = 'body > div.style-modalbox-3IBZd > div.style-mask-23bQN.dialog-mask > div > div > div > div > div.style-fte-texts-KTJf0 > button';
                    CALLME_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div > div.style-footer-i5XuL > div.style-center-panel-zj-lW > ul > li.callme-item';
                    DDI_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-pinned-2EhTu.style-country-pOm2s > div > div.style-content-3Z52b > input';
                    NUMBER_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-content-3Z52b.style-input-3lf_O > input';
                    break;
                case '2':
                    AUDIO_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-audio-select-box-1LOYQ > button';
                    CALLME_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-content-3Z52b.ellipsis > button';
                    SKIP_SELECTOR = 'body > div.style-modalbox-3IBZd > div.style-mask-23bQN.dialog-mask > div > div > div > div > div.style-fte-texts-KTJf0 > button';
                    DDI_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-pinned-2EhTu.style-country-pOm2s > div > div.style-content-3Z52b > input';
                    NUMBER_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div.style-controlbar-box-3rR8K > div.style-footer-i5XuL.style-footer-control-bar-r4EIo > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-content-3Z52b.style-input-3lf_O > input';

                    break;
                case '3':
                    AUDIO_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div > div.style-footer-i5XuL > div.style-audio-select-box-1LOYQ > button';

                    SKIP_SELECTOR = '#welcome_skip';

                    CALLME_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div > div.style-footer-i5XuL > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div.style-box-3Jg8j.style-row-1BHDk > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-content-3Z52b.ellipsis > button';

                    DDI_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div > div.style-footer-i5XuL > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div.style-box-3Jg8j.style-row-1BHDk > div.style-content-3Z52b.style-combobox-2TXq8 > div > div > div.style-pinned-2EhTu.style-country-pOm2s > div > div.style-content-3Z52b > input';
                    NUMBER_SELECTOR = '#meetingSimpleContainer > div.style-content-container-YOy1G > div > div.style-footer-i5XuL > div.style-center-panel-zj-lW > ul > li.callme-item > div > div > div.style-box-3Jg8j.style-row-1BHDk > div.style-content-3Z52b.style-combobox-2TXq8 > div > div.style-box-3Jg8j.style-row-1BHDk.style-edit-content-2HIHF > div.style-content-3Z52b.style-input-3lf_O > input';
                    break;
            }
            await page.setDefaultNavigationTimeout(0);
            await page.goto(data.url, {
                waitUntil: 'networkidle2'
            });
            const elementHandle = await page.$(
                'iframe[name="thinIframe"]',
            );
            const frame = await elementHandle.contentFrame();
            const input_email = await Promise.race([
                new Promise(resolve => setTimeout(() => resolve(), 5000)),
                frame.waitForSelector(EMAIL_INPUT, { visible: true })
            ]);
            if (input_email) {
                await frame.type(EMAIL_INPUT, +generate(10) + 'a@a.com');
            }
            const input_name = await Promise.race([
                new Promise(resolve => setTimeout(() => resolve(), 1000)),
                frame.waitForSelector(NAME_INPUT, { visible: true })
            ]);
            if (input_name) {
                await frame.type(NAME_INPUT, '+' + generate(4) + '****00');
            } else {
                await frame.type(NAME_INPUT2, '+' + generate(4) + '****00');
            }    
            await frame.click(BUTTON_SELECTOR);
            //await frame.waitForSelector(SKIP_SELECTOR);
            const agree_button = await Promise.race([
                new Promise(resolve => setTimeout(() => resolve(), 2000)),
                frame.waitForSelector(BUTTON_AGREE, { visible: true })
            ]);
            if (agree_button) {
                await frame.click(BUTTON_AGREE);
            }
            //await frame.click(SKIP_SELECTOR);
            //await frame.waitForSelector(AUDIO_SELECTOR, { timeout: 0 });
            //await frame.click(AUDIO_SELECTOR);
            await frame.waitForSelector(CALLME_SELECTOR);
            await frame.click(CALLME_SELECTOR);
            await frame.waitForSelector(DDI_SELECTOR, { timeout: 0 });
            await frame.click(DDI_SELECTOR, { clickCount: 3 });
            await frame.type(DDI_SELECTOR, ddi);
            //await page.waitForTimeout(400000)
            await frame.waitForSelector(NUMBER_SELECTOR, { timeout: 0 });

            await frame.type(NUMBER_SELECTOR, call_number, { delay: 50 });
            const selector_number = await Promise.race([
                new Promise(resolve => setTimeout(() => resolve(), 200)),
                frame.waitForSelector(NUMBER_SELECTOR, { visible: true })
            ]);
            if (selector_number) {
                await frame.type(NUMBER_SELECTOR, String.fromCharCode(13));
            } else {

            }

            await frame.waitForSelector(MUTE_SELECTOR, { timeout: 0 });
            await frame.click(MUTE_SELECTOR);
            await frame.waitForSelector(BUTTON_ENTER_SELECTOR);
            await frame.click(BUTTON_ENTER_SELECTOR);

            const info_meeting = await Promise.race([
                new Promise(resolve => setTimeout(() => resolve(), 2000)),
                frame.waitForSelector(INFO_MEETING, { visible: true })
            ]);
            if (info_meeting) {
                return
            } else {

            }
            await frame.waitForSelector(CALL_DIALOG, { timeout: 70000 });
            
            await frame.waitForSelector(CALL_DIALOG, { hidden: true, timeout: 70000 })
        });
        router.post('/', async function (req, res) {
            number = req.body.number;
            url = req.body.url;
            count = req.body.count;
            var call = {
                url,
                count,
                number
            }
            const result = await cluster.execute(call)

            res.writeHead(200);
            res.end(result);
        });
    } catch (err) {
        console.log(err);
    }
})();
router.get('/', callController.sendCall);
//router.get('/makeCall', callController.makeCall);
module.exports = router;