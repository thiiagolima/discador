const Piscina = require('piscina');
const path = require('path');
const { post } = require('../routes/meeting-routes');

function range(start, stop, step) {
    if (typeof stop == 'undefined') {
        // one param defined
        stop = start;
        start = 0;
    };
    if (typeof step == 'undefined') {
        step = 1;
    };
    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    };
    return result;
};

exports.searchValids = async (req, res) => {
    res.render('searchValids.ejs')
}
exports.validsRoutine = async (req, res) => {
    let id = req.body.id;
    let empresa = req.body.empresa;
    let initialId = parseInt(id + '0000000');
    let finalId = parseInt(id + '9999999');
    rangeId = range(initialId, finalId)
    const piscina = new Piscina({
        filename: path.resolve(__dirname, '../workers/worker.js'),
        resourceLimits: {
            maxOldGenerationSizeMb: 16,
            maxYoungGenerationSizeMb: 4,
            codeRangeSizeMb: 16
        }
    });

    (async function () {
        for (var i = 0; i < rangeId.length; i++) {
            line = rangeId[i];
            const result = await piscina.runTask({ line, empresa });
            console.log(result);
        }
    })();

}